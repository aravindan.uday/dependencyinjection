package com.android.dependencyinjection

import com.android.dependencyinjection.house.Boltons
import com.android.dependencyinjection.house.Starks
import dagger.Component
import javax.inject.Inject

class War @Inject constructor(private var starks: Starks, private var boltons: Boltons) {

    fun prepare() {
        starks.prepareForWar()
        boltons.prepareForWar()
    }

    fun report () {
        starks.reportForWar()
        boltons.reportForWar()
    }

    @Component
    internal interface BattleComponent {
        val war: War
        val boltons : Boltons
        val starks : Starks
    }
}

fun main(args : Array<String>) {
    val war = DaggerWar_BattleComponent.create().war
    war.prepare()
    war.report()
}