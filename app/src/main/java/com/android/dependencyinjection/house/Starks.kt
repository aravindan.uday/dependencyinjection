package com.android.dependencyinjection.house

import com.android.dependencyinjection.House
import javax.inject.Inject

class Starks @Inject constructor() : House {

    override fun prepareForWar() {
        System.out.println(this.javaClass.simpleName +" prepared for war")
    }

    override fun reportForWar() {
        System.out.println(this.javaClass.simpleName + " reporting..")
    }
}