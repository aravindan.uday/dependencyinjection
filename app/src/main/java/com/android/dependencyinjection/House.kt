package com.android.dependencyinjection

interface House {
    fun prepareForWar()
    fun reportForWar()
}